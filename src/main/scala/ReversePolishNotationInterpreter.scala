import scala.io.Source
import scala.collection.mutable.{ArrayBuffer, ListBuffer, Stack}
import scala.util.control.Exception.allCatch
import scala.math._


object ReversePolishNotationInterpreter {

  def main(args : Array[String]): Unit ={
    /*val filename = "RPN.in"
    val lines = Source.fromFile(filename)
    val answer = interpret(lines.getLines().next())*/

    val questions = List (
      "15 7 1 1 + - ÷ 3 x 2 1 1 + + -",
      "4 2 5 * + 1 3 2 * + /",
      "2 5 * 4 + 3 2 * 1 + /",
      "5 8 2 15 * sin * + 2 45 tan + /",
      "2 15 * sin 8 * 5 + 45 tan 2 + /",
      "3 e 2 ^ ln * 8 60 cos * + 3 4 0.5 ^ * 1 - /",
      "e 2 ^ ln 3 * 60 cos 8 * + 4 0.5 ^ 3 * 1 - /"
    )

    val answers = List (5.0,2.0,2.0,3.0,3.0,2.0,2.0)

    val interpreted = questions.map{question =>
      interpret(question).get
    }

    println(interpreted)
    println(answers)

    val algs = List(
      "((15 ÷ (7 - (1 + 1))) × 3) - (2 + (1 + 1))"
    )

    val check = algToRPN("((15÷(ln(7) - (e+ 1))) × 3) - ( 2 +(1 + sin(1)))")
    println(check)
    println(interpret(check))
  }

  def interpret(expression : String): Option[Double] = {
    val expressionList = expression.split(" ")
    var stack = Stack[Double]()
    expressionList.foreach{ el =>
      if (isNum(el)){
        stack.push(el.toDouble)
      }else{
        el match{
          case "+" => stack.push(stack.pop + stack.pop)
          case "-" => stack.push(-stack.pop + stack.pop)
          case "/" | "÷" =>
            val first = stack.pop
            val second = stack.pop
            stack.push(second / first)
          case "*" | "x" | "×" => stack.push(stack.pop * stack.pop)
          case "^" =>
            val first = stack.pop
            val second = stack.pop
            stack.push(pow(second, first))
          case "sin" => stack.push(sin(stack.pop.toRadians))
          case "cos" => stack.push(cos(stack.pop.toRadians))
          case "tan" => stack.push(tan(stack.pop.toRadians))
          case "ln" => stack.push(log(stack.pop))
          case "e" => stack.push(E)
          case _ => println("err at reading " + el)
        }
      }
    }
    return Option(stack.pop)
  }


  def isNum(s : String): Boolean = { (allCatch opt s.toDouble).isDefined }



  def cleanAlgStack(stack : Stack[Char],letterStack : Stack[Char]): ListBuffer[Char] ={
    val retList = new ListBuffer[Char]
    for (elem <- stack) {
      if (!((elem >= 'a' && elem <= 'z') || (elem >= 'A' && elem <= 'B'))) { //if the operator is not a letter, append a space
        retList += ' '
        while (letterStack.nonEmpty) {
          retList += letterStack.pop
        }
        retList += ' '
        retList += elem
        //retList += ' '
      } else {
        letterStack.push(elem)
      }
    }
    retList
  }

  def algToRPN(algExpression : String): String ={ //cant deal with mutli character operators yet, MAYBE can, doesnt deal with whitespace correctly yet
    var newExpression : ListBuffer[Char] = new ListBuffer[Char]()
    var stackList : ListBuffer[Stack[Char]] = new ListBuffer[Stack[Char]]()
    val letterStack = new Stack[Char]
    stackList+=new Stack[Char]

    algExpression.foreach{ c =>
      if (c == '('){ //if the character is an opening bracket, we want to start a new stack
        stackList+=new Stack[Char]
      }

      else if (c == ')'){ //if the character is a closing bracket, we want to place all the operators remaining on the current stack, then remove it
        val operatorStack = stackList.remove(stackList.size - 1)
        newExpression++=cleanAlgStack(operatorStack,letterStack)
      }

      else if ((c >= '0' && c <= '9' ) || c == 'e'){ // if the character is a number or a space OR a constant like e , just put it on the start of the string
        newExpression+=c
      }

      else if ( c != ' ') { //the character is an operator
        val operatorStack = stackList.remove(stackList.size - 1)
        newExpression+=' '
        while (operatorStack.nonEmpty && getOrdering(c) < getOrdering(operatorStack.top)) {
          newExpression += operatorStack.pop
        }

        operatorStack.push(c)
        stackList+=operatorStack
      }
    }

    //clean the stack again
    if (stackList.nonEmpty){
      val operatorStack = stackList.remove(stackList.size-1)
      newExpression++=cleanAlgStack(operatorStack,letterStack)
    }

    //clean any leftover letters (if the final operator is something like sin or cos)
    while (letterStack.nonEmpty){
      newExpression+=letterStack.pop
    }


    newExpression.mkString.trim.replaceAll(" +"," ")
  }



  def getOrdering(c : Char) : Int = {
    if (c == '*' || c == '/' || c == '%' || c == 'x' || c == '÷' || c == '×'){
      2
    }else if ( c == '+' || c == '-'){
      1
    }else{
      0
    }
  }

}




